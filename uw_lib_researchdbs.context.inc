<?php
/**
 * @file
 * File: uw_lib_researchdbs.context.inc.
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_lib_researchdbs_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'research_databases';
  $context->description = 'Search and results pages';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'find-and-use-resources/research-databases' => 'find-and-use-resources/research-databases',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_lib_researchdbs-searchform' => array(
          'module' => 'uw_lib_researchdbs',
          'delta' => 'searchform',
          'region' => 'content',
          'weight' => '40',
        ),
        'uw_lib_researchdbs-alerts' => array(
          'module' => 'uw_lib_researchdbs',
          'delta' => 'alerts',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Search and results pages');
  $export['research_databases'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'research_databases_plainpages';
  $context->description = 'Standalone pages within research databases application';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'find-and-use-resources/research-databases/browse' => 'find-and-use-resources/research-databases/browse',
        'find-and-use-resources/research-databases/alerts' => 'find-and-use-resources/research-databases/alerts',
        'find-and-use-resources/research-databases/trials' => 'find-and-use-resources/research-databases/trials',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_lib_researchdbs-about' => array(
          'module' => 'uw_lib_researchdbs',
          'delta' => 'about',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Standalone pages within research databases application');
  $export['research_databases_plainpages'] = $context;

  return $export;
}
