<?php
/**
 * @file
 * File: uw_lib_researchdbs.strongarm.inc.
 */

/**
 * Implements hook_strongarm().
 */
function uw_lib_researchdbs_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_taxonomy_term__lib_rdb_departments';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'metatags' => array(
          'weight' => '40',
        ),
        'path' => array(
          'weight' => '30',
        ),
        'redirect' => array(
          'weight' => '30',
        ),
        'name' => array(
          'weight' => '-5',
        ),
        'description' => array(
          'weight' => '0',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_taxonomy_term__lib_rdb_departments'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_taxonomy_term__lib_rdb_departments';
  $strongarm->value = TRUE;
  $export['metatag_enable_taxonomy_term__lib_rdb_departments'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_lib_rdb_departments';
  $strongarm->value = 'lib_rdb_departments';
  $export['uuid_features_entity_taxonomy_term_lib_rdb_departments'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_file_taxonomy_term_lib_rdb_departments';
  $strongarm->value = 'lib_rdb_departments';
  $export['uuid_features_file_taxonomy_term_lib_rdb_departments'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_lib_researchdbs_api_alerts';
  $strongarm->value = 'https://libtextura.uwaterloo.ca/api-auth/v1/platforms/messages/';
  $export['uw_lib_researchdbs_api_alerts'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_lib_researchdbs_api_alldbs';
  $strongarm->value = 'https://libtextura.uwaterloo.ca/api-auth/resource_profiles/1/';
  $export['uw_lib_researchdbs_api_alldbs'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_lib_researchdbs_api_dept';
  $strongarm->value = 'https://libtextura.uwaterloo.ca/api-auth/resource_profiles/';
  $export['uw_lib_researchdbs_api_dept'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_lib_researchdbs_api_trials';
  $strongarm->value = 'https://libtextura.uwaterloo.ca/api-auth/v1/resource_trials/';
  $export['uw_lib_researchdbs_api_trials'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_lib_researchdbs_app_path';
  $strongarm->value = 'find-and-use-resources/research-databases';
  $export['uw_lib_researchdbs_app_path'] = $strongarm;

  return $export;
}
