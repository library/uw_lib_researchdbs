<?php
/**
 * @file
 * File: uw_lib_researchdbs.features.uuid_term.inc.
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_lib_researchdbs_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Public Service',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '0852b875-f87a-43ca-8925-2cd72d55cb0b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 73,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Women\'s Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '0c98f633-a897-4962-a688-1d78d0096f6a',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 68,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Global Governance',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '0f4ebd53-d69c-437b-82c1-33c636333372',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 34,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'East Asian Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '135935fb-0ace-4f44-a5f5-9e3514b38310',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 21,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Sexuality, Marriage and Family Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '1dd98137-3ce9-4dd0-b063-56a394408ab2',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 60,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Chemical Engineering',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '2274aabc-ba21-4ec4-bbc1-77d8278d159c',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 14,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Fine Arts',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '2a6d6b50-8e72-477b-bf72-dacc859170c7',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 29,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Statistics and Actuarial Science',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '2b2f3a5a-32f6-4be2-b4ec-096defcd56a7',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 7,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Sociology',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '34672687-8e9e-434f-9696-32587a15a09d',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 62,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Knowledge Integration',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '37e8a276-9141-442c-a7c4-345c218bc77c',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 40,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Environment, Resources and Sustainability',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '39b5289c-959e-4aa7-9e48-d615b5ae8db9',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 27,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Environment and Business',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '9f7c696f-840a-4dba-a018-db01fcceedab',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 78,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Physics',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '3a7de2f9-6ad3-49cb-9a97-bb3a6d000207',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 76,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Biology',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '4480ed17-81e7-4645-9ff7-60a7c57b65c0',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 11,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Standards',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '45de461f-b72c-4e11-a8a1-a7cd78959c3c',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_ctypes',
    'field_ctype_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 88,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Mathematics',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '49ba7b9a-abfe-4a78-8f15-25aeb3f254a8',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 44,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Economic Development',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '4a99f39a-06d2-41ed-adf3-18311d671863',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 79,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Kinesiology',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '5438dc71-66ff-4638-adb8-b8cf5757c69f',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 39,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Applied Mathematics',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '5a1797a4-3cb8-4584-9a09-60d1c833f6f0',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 9,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'French Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '5dc1bdf3-f3e9-46b9-a195-ffc20e65a564',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 30,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Electrical and Computer Engineering',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '5ee748cd-8fc8-4ad4-a99b-a8f81a3cca3b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 23,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Combinatorics and Optimization',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '5eeed85f-a138-4ff0-899a-0dee1f52c9e2',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 17,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Recreation and Leisure Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '634b8ba5-23ab-4ce2-8c5e-f901d2559409',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 57,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Studies in Islam',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '66df2ab1-f076-4a0c-b4a9-21f25010f2ea',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 66,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Primary Sources',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '6dbf8b6e-25e3-42b5-9c0a-63fd5f5b399b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_ctypes',
    'field_ctype_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 87,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Library Science',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '6ea7d812-c9ae-4f90-a424-0aa1807e88eb',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 41,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Patents',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '73b937d6-d464-4e85-931c-6da2cb4d09a0',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_ctypes',
    'field_ctype_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 86,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Optometry',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '796fdf96-710e-4e9a-8547-0aa4de9e7003',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 48,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Medieval Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '7c054ed3-20d2-4493-8060-7714b2dc8197',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 46,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Entrepreneurship',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '7d3e6c91-55de-46fc-bd42-3cd0050109d9',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 25,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Film Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '83aadf68-abef-4c01-99b8-c0250d8e1f9c',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 28,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Computer Science',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '86a16fa8-f940-472b-af44-b06288bb3444',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 18,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Data and Statistics',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '8be51d53-bfe0-436f-b71f-8099d007b2f7',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_ctypes',
    'field_ctype_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 82,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'History',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '8c8a3bd2-872a-4b26-9c60-000702e1dc53',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 36,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Peace and Conflict Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '8caf5119-bd4b-42bb-a801-9a6d7229b099',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 49,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'English',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '93082aba-6ef4-42d6-8325-c326cc2cc02b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 24,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Spanish and Latin American Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '977faaee-78f6-42e2-959b-139e1796be33',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 63,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'International Development',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '98f2d25c-40ae-473f-b198-6a62b854e8a6',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 37,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Psychology',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '99d0c75d-0e7b-499e-bf5e-571200eff904',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 54,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Russian Language and Culture',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'a04a5228-77d8-4e07-8d83-351c729bf14e',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 59,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Political Science',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'a14ca983-5afa-4caf-8fb8-40f909fce97f',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 53,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Catholic Thought',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'a4b57681-59e4-4844-bdc2-7e64f947b716',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 13,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Drama',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'a84b746d-febc-4c55-b389-4656cbbc6c5b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 19,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Dissertations and Theses',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'abf1050f-4db3-4250-88f3-cab044275728',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_ctypes',
    'field_ctype_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 83,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Public Health and Health Systems',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'ad40b605-122d-48da-9c97-ad966eb3afc9',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 55,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Jewish Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'ada85f79-94f5-4fcf-9a5c-ff0954a7ea7c',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 38,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Civil and Environmental Engineering',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'ae9f8e94-3775-48ee-99a3-0ca75d00d711',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 15,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Anthropology',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'b10bcf0d-0183-4521-a4cb-95c470de9aaa',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 8,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Video and Images',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'b1b89d7a-943b-442f-b651-f94d1f36d129',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_ctypes',
    'field_ctype_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 89,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Mechanical and Mechatronics Engineering',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'b2174ea9-02c8-49dd-afd4-8d52aa3dc50e',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 45,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Geography and Environmental Management',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'b9714791-c0ff-4c5c-b402-c5f40e98406e',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 31,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Science and Business',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'babbf8d6-bdf3-438f-bb86-07cdcfcb64be',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 77,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Classical Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'bba3af7a-b357-472e-a397-68566b5710a5',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 16,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Business',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'c0131ef6-4e13-4a38-b99e-e14bbbb53a96',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 69,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Music',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'c27ae0c5-eb9a-43ba-9b0c-f91cca9c0bb7',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 47,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'German Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'c34680cd-81ee-4914-87c4-5b74680de993',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 33,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Planning',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'c5c4c354-a736-4aec-a3b6-87246cb28524',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 52,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'News Sources',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'c77a115d-a33b-4947-87f8-d4f0c9b0e4a0',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_ctypes',
    'field_ctype_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 85,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Earth and Environmental Sciences',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'c7acf2ff-b4f9-4ccd-b962-6bd099e652f2',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 20,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Accounting and Finance',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'cd0b16d6-6618-4096-a0c6-c27270114c76',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 6,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Architecture',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'cf21c2b8-3e71-4635-a7e3-b057b701cede',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 10,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Religious Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'd1a24118-7160-45c1-891e-f5313d2a10ec',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 58,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Speech Communication',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'd2c6898e-43d9-4f0f-818b-f4886d1cd05e',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 74,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Sustainability Management',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'd6ff5ca7-8736-4d41-99c7-1a39fa8463fd',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 80,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Biomedical Engineering',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'd942993e-ec6c-4d1a-bd70-0557f4467a29',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 12,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Government Information',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'dd40d6b7-901e-460a-b97b-cb011cb53640',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 35,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Legal Studies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'df72cecf-a7a9-4b3d-b7e7-0a9a6609ec8f',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 71,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Social Work',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'e41fec2f-ac62-4435-87b5-6a6223f0a332',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 61,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Philosophy',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'ea9f1c68-7a93-4a81-95f4-94386df964ab',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 50,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Management Sciences',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'eae1d48a-d817-40b6-926a-c2bfdf341afe',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 43,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Book and Film Reviews',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'ee49dac9-7503-4d26-bf20-cdaf48fc82e5',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_ctypes',
    'field_ctype_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 81,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Economics',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'f038162c-8cbc-4069-8260-197d23285747',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 22,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Pure Mathematics',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'f0f18bd0-2ffd-4604-a518-5b3a218b112f',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 56,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Chemistry',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'f13383d8-ea3c-44c9-93dc-1404c4635551',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 5,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => '* Multidisciplinary databases',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'f1de4c6c-b0c4-495b-9e69-5987f95a22db',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 70,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Astronomy',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'fc752464-d992-4cab-bda4-cadbe8fd4e7a',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 51,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Pharmacy',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'fce00103-6c14-4d88-9a83-68015e647481',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 3,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Systems Design Engineering',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'fd70e6d1-6b52-4026-807f-10b4ef0477dc',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'lib_rdb_departments',
    'field_textura_profile_id' => array(
      'und' => array(
        0 => array(
          'value' => 67,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
