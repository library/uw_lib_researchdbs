INTRODUCTION
------------
The Library Research Databases Module creates pages at <site>/find-and-use-resources/research-databases
which provides a search interface to the library's research databases.

The module queries the Textura application (https://libtextura.uwaterloo.ca) APIs to retrieve research 
databases based on the search parameters.

The module creates three Drupal taxonomies in order to manage the list of departments/programs and
the content types. Each dept/program is assigned its matching Textura resource profile ID.
Each content type listed in the search form is mapped to the corresponding content type(s) in Textura. 


REQUIREMENTS 
------------

- The following modules are required in order to install this module successfully:
  - context
  - entityreference
  - features
  - metatag
  - number
  - options
  - taxonomy
  - tvi
  - uuid
  - uuid_features


INSTALLATION
------------

- Download the module from https://git.uwaterloo.ca/library/uw_lib_researchdbs
  into your site's modules directory, and enable the module.
- Run update.php so that the hook_update() function updates the respective
  taxonomy terms with their correct information and references.


CONFIGURATION
-------------

- None required. 

Note: 
- The module automatically creates three Drupal taxonomies and populates them
  with the required taxonomy terms.


MAINTAINERS
-----------

- The module is maintained by Graham Faulkner (graham.faulkner@uwaterloo.ca)
  and the Digital Initiatives department of the UWaterloo Library.


