/**
 * @file
 * Custom JavaScript for the module
 * uw_lib_researchdbs.js
 */

(function ($) {

  $(document).ready(function() {

    // remove the "loading" spinner on page load, if present
    // (i.e. user returns to this page from a database)
    if ($('html')[0].classList.contains("loading")) {
      $('html')[0].classList.remove("loading");
    }

    // handle clicking of "more info" links
    var moreInfoLinks = $('.more-info-toggle');
    moreInfoLinks.click(function() {
      var moreInfoLink = $(this);
      var moreInfoContent = moreInfoLink.closest("p").next("div");
      moreInfoContent.toggle();
      if (moreInfoContent.is(":visible")) {
        moreInfoLink.text("View fewer details");
      } 
      else {
        moreInfoLink.text("View more details");
      }
    });

    // handle selections in the db name textfield autocomplete
    $('#uw_lib_researchdbs_keywords').on('autocompleteSelect', function (event) {
      // need to get the resource id before we change the value in input.
      // The .html() returns something like:
      //   '<div>Name of Database<span class="database-resource-id">999</span></div>'
      // Extract the resource ID from the <span>...</span>
      var dbNameAndId = $('div#autocomplete ul li[class="selected"]').html();
      var idxOfSpan = dbNameAndId.indexOf("<span "); 
      // start at 5 to skip starting "<div>" tag in string
      var dbName = dbNameAndId.substring(5, idxOfSpan - 1);
      var resource_id = parseInt($('div#autocomplete ul li[class="selected"] span')[0].innerText);

      // put the text of the selection in the input 
      $(this).val(dbName);
      redirectToDatabase(resource_id);
    });

    // if typing database name, handle keypresses and especially Enter key.
    // Important to have 'keyup' AND 'keypress' handlers, for IE/Edge compatibility.
    $('#uw_lib_researchdbs_keywords').on('keyup keypress', function (event) {

      // if they pressed the Enter key
      if (event.which == 13) {

        // if there are no matches presently, don't redirect
        if ($('div#autocomplete ul li div').first().html().indexOf("No results found.") == 0) {
          event.preventDefault();
          $('#uw_lib_researchdbs_keywords').focus();
          return false;
        } 
        else {
          var dbNameAndId = "";
          var dbName = "";
          var resource_id = 0;

          if ($('div#autocomplete ul li[class="selected"]').length == 1) {
            // user is selecting item within autocomplete dropdown with mouse or keyboard
            dbNameAndId = $('div#autocomplete ul li[class="selected"]').html();
            idxOfSpan = dbNameAndId.indexOf("<span "); 
            dbName = dbNameAndId.substring(5, idxOfSpan - 1);
            resource_id = parseInt($('div#autocomplete ul li[class="selected"] span')[0].innerText);
          }
          else {
            // user pressed Enter when typing in textfield; choose top item in autocomplete
            dbNameAndId = $('div#autocomplete ul li')[0].innerHTML;
            idxOfSpan = dbNameAndId.indexOf("<span "); 
            dbName = dbNameAndId.substring(5, idxOfSpan - 1);
            resource_id = parseInt($('div#autocomplete ul li span')[0].innerText);
          }
          // put the text of the selection in the input 
          $('#uw_lib_researchdbs_keywords').val(dbName);
          redirectToDatabase(resource_id);
        }
      }
    });

  });

  // redirect to the specific database's website
  function redirectToDatabase(resource_id) {
    $('html').addClass("loading");
    var current_url = window.location.href;
    var locator = current_url.indexOf("research-databases");
    var base_url = current_url.substr(0, locator + "research-databases".length);
    window.location = base_url + '/id/' + resource_id;
  }

})(jQuery);
