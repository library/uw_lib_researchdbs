<?php
/**
 * @file
 * File: uw_lib_researchdbs.features.taxonomy.inc.
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_lib_researchdbs_taxonomy_default_vocabularies() {
  return array(
    'lib_rdb_ctypes' => array(
      'name' => 'Library - Research Databases - content types',
      'machine_name' => 'lib_rdb_ctypes',
      'description' => 'Content types listed in WCMS Research Databases search form',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'base_i18n_mode' => 0,
      'base_language' => 'und',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'lib_rdb_departments' => array(
      'name' => 'Library - Research Databases - departments and programs',
      'machine_name' => 'lib_rdb_departments',
      'description' => 'list of departments and programs (subject headings in Textura) to use in research databases filtering',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'base_i18n_mode' => 0,
      'base_language' => 'und',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
