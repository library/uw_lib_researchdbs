<?php
/**
 * @file
 * File: uw_lib_researchdbs.features.user_permission.inc.
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_lib_researchdbs_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer research databases'.
  $permissions['administer research databases'] = array(
    'name' => 'administer research databases',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_lib_researchdbs',
  );

  // Exported permission: 'define view for terms in lib_rdb_ctypes'.
  $permissions['define view for terms in lib_rdb_ctypes'] = array(
    'name' => 'define view for terms in lib_rdb_ctypes',
    'roles' => array(),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for terms in lib_rdb_departments'.
  $permissions['define view for terms in lib_rdb_departments'] = array(
    'name' => 'define view for terms in lib_rdb_departments',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary lib_rdb_ctypes'.
  $permissions['define view for vocabulary lib_rdb_ctypes'] = array(
    'name' => 'define view for vocabulary lib_rdb_ctypes',
    'roles' => array(),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary lib_rdb_departments'.
  $permissions['define view for vocabulary lib_rdb_departments'] = array(
    'name' => 'define view for vocabulary lib_rdb_departments',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'delete terms in lib_rdb_ctypes'.
  $permissions['delete terms in lib_rdb_ctypes'] = array(
    'name' => 'delete terms in lib_rdb_ctypes',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in lib_rdb_departments'.
  $permissions['delete terms in lib_rdb_departments'] = array(
    'name' => 'delete terms in lib_rdb_departments',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in lib_rdb_ctypes'.
  $permissions['edit terms in lib_rdb_ctypes'] = array(
    'name' => 'edit terms in lib_rdb_ctypes',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in lib_rdb_departments'.
  $permissions['edit terms in lib_rdb_departments'] = array(
    'name' => 'edit terms in lib_rdb_departments',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
