<?php
/**
 * @file
 * File: uw_lib_researchdbs.features.field_base.inc.
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_lib_researchdbs_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_ctype_textura_profile_id'.
  $field_bases['field_ctype_textura_profile_id'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_ctype_textura_profile_id',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_textura_profile_id'.
  $field_bases['field_textura_profile_id'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_textura_profile_id',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  return $field_bases;
}
