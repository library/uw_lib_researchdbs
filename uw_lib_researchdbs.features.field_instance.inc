<?php
/**
 * @file
 * File: uw_lib_researchdbs.features.field_instance.inc.
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_lib_researchdbs_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'taxonomy_term-lib_rdb_ctypes-field_ctype_textura_profile_id'.
  $field_instances['taxonomy_term-lib_rdb_ctypes-field_ctype_textura_profile_id'] = array(
    'bundle' => 'lib_rdb_ctypes',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_ctype_textura_profile_id',
    'label' => 'Textura profile ID',
    'required' => FALSE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 41,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-lib_rdb_departments-field_textura_profile_id'.
  $field_instances['taxonomy_term-lib_rdb_departments-field_textura_profile_id'] = array(
    'bundle' => 'lib_rdb_departments',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_textura_profile_id',
    'label' => 'Textura profile ID',
    'required' => FALSE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 41,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Textura profile ID');

  return $field_instances;
}
