<?php
/**
 * @file
 * File: uw_lib_researchdbs.features.menu_links.inc.
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_lib_researchdbs_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: admin/config/system/uw_lib_researchdbs_settings.
  $menu_links['menu-site-management_research-databases-settings:admin/config/system/uw_lib_researchdbs_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_lib_researchdbs_settings',
    'router_path' => 'admin/config/system',
    'link_title' => 'Research databases settings',
    'options' => array(
      'attributes' => array(
        'title' => 'Configure the path and API urls for the research databases module.',
      ),
      'identifier' => 'menu-site-management_research-databases-settings:admin/config/system/uw_lib_researchdbs_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: admin/structure/taxonomy/lib_rdb_ctypes.
  $menu_links['menu-site-manager-vocabularies_research-databases---content-types:admin/structure/taxonomy/lib_rdb_ctypes'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/lib_rdb_ctypes',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Research databases - content types',
    'options' => array(
      'attributes' => array(
        'title' => 'Content types for the drop down list',
      ),
      'identifier' => 'menu-site-manager-vocabularies_research-databases---content-types:admin/structure/taxonomy/lib_rdb_ctypes',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: admin/structure/taxonomy/lib_rdb_departments.
  $menu_links['menu-site-manager-vocabularies_research-databases---departments-and-programs:admin/structure/taxonomy/lib_rdb_departments'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/lib_rdb_departments',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Research databases - departments and programs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_research-databases---departments-and-programs:admin/structure/taxonomy/lib_rdb_departments',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Research databases - content types');
  t('Research databases - departments and programs');
  t('Research databases settings');

  return $menu_links;
}
